## Glossary ##

List any technical terms to be used in this specification.

## What ##

What is this feature?

## Why ##

Why are we developing this feature?

## Requirements ##

What is required of this feature?

**Include** "does not do x, y, z" 

## Who ##

Who will develop this feature?

Who will use this feature?

## When ##

When will this feature be developed?

## How ##

High-level overview of how we will implement this feature.# 

## Spec Test ##

Link to testing instructions or test plan

## Rev Table ##

| Version | Date     | Author    | Note         |
|---------|----------|-----------|--------------|
| 1       | 1/6/2021 | Bgraybill | Initial spec |
