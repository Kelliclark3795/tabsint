package com.creare.tabsintnative;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
//import android.content.Intent;
//import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Handler;
import android.webkit.WebView;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

// for Immersive view...
import android.view.View;
import android.view.WindowManager;

// for Volume buttons
import android.media.AudioManager;

// for intent send data to another app
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//for handling throws IOException
import java.io.*;

import android.provider.MediaStore;
import android.provider.DocumentsContract;
import android.database.Cursor;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.net.Uri;

// Usb imports
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import java.util.HashMap;

// Flic imports
// NOTE: commenting out flic functionality from 47937026b086a035e8acf764551cbfe54d6435b5
// Uncomment all changes from that commit to re-enable this, but for now it is preventing
// TabSINT from building for target Android API 30 (Android 11).
// Some more information about this problem here: https://gitlab.com/creare-com/tabsint/-/issues/598
// and here: https://gitlab.com/creare-com/tabsint/-/merge_requests/246
// import io.flic.flic2libandroid.Flic2Manager;
// import io.flic.flic2libandroid.Flic2ButtonListener;
// import io.flic.flic2libandroid.Flic2ScanCallback;
// import io.flic.flic2libandroid.Flic2Button;

import android.content.ContentResolver;
import android.webkit.MimeTypeMap;

import java.io.FileDescriptor;
/**
 * This calls out to the ZXing barcode reader and returns the result.
 *
 * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
 */
public class TabSINTNative extends CordovaPlugin {
  public static final String ACTION_INITIALIZE = "initialize";
  public static final String ACTION_RESET_AUDIO = "resetAudio";
  public static final String ACTION_SET_AUDIO = "setAudio";
  public static final String ACTION_GET_AUDIO_VOLUME = "getAudioVolume";
  public static final String ACTION_SEND_EXTERNAL_DATA = "sendExternalData";
  public static final String ACTION_GET_EXTERNAL_DATA_INTENT = "getExternalDataIntent";
  public static final String ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER = "setExternalDataIntentHandler";
  public static final String ACTION_REGISTER_USB_DEVICE_LISTENER = "registerUsbDeviceListener";
  public static final String ACTION_UNREGISTER_USB_DEVICE_LISTENER = "unregisterUsbDeviceListener";
  //this line is definitely needed
  public static final String ACTION_GET_EXTERNAL_STORAGE_DIR = "getExtStorageDir";
  public static final String ACTION_COPY_EXTERNAL_STORAGE_FILES = "copyExtStorageFiles";
  public static final String ACTION_DELETE_COPIED_INTERNAL_DIR = "deleteCopiedInternalDir";

  // public static final String ACTION_SCAN_FLIC_BUTTON = "scanFlicButton";
  // public static final String ACTION_DISCONNECT_FLIC_BUTTON = "disconnectFlicButton";
  // public static final String ACTION_GET_CONNECTED_FLIC_BUTTONS = "getConnectedFlicButtons";

  private static final int REQUEST_CODE_OPEN_DOCUMENT_TREE = 3;
  private static final String TAG = "ReceiverPlugin";

  private CallbackContext onNewIntentCallbackContext = null;
  private CallbackContext volumeCallbackContext = null;
  private CallbackContext openDirectoryCallbackContext = null;
  private CallbackContext copyExtStorageFilesCallbackContext = null;
  private CallbackContext deleteCopiedInternalDirCallbackContext = null;



  // Usb device related variables
  private UsbManager mUsbManager;
  private UsbDevice mUsbDevice;
  private boolean mUsbConnected = false;
  private BroadcastReceiver mUsbReceiver;
  private CallbackContext mUsbDeviceCallbackContext = null;
  private CordovaInterface mCordova;
  private IntentFilter mFilter;

  private AudioManager mAudioManager;

  // Flic variables
  private boolean isScanning;
  // private Flic2Manager flicManager = null;
  // private CallbackContext flicCallbackContext = null;

  /**
   * Constructor.
   */
  public TabSINTNative() {
  }


  /**
   * Sets the context of the Command. This can then be used to do things like
   * get file paths associated with the Activity.
   *
   * @param cordova The context of the main Activity.
   * @param webView The CordovaWebView Cordova is running in.
   */
  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    mUsbManager = (UsbManager)cordova.getActivity().getSystemService(Context.USB_SERVICE);
    mCordova = cordova;
    // flicManager = Flic2Manager.initAndGetInstance(mCordova.getActivity().getApplicationContext(), new Handler());
    mFilter = new IntentFilter();
    mFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
    mFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
    mUsbReceiver = new BroadcastReceiver() {
      public void onReceive(Context context, Intent intent) {
          String action = intent.getAction();
          if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
              mUsbDevice = null;
              sendUsbDeviceInfo();
          } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
              mUsbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
              sendUsbDeviceInfo();
          }
      }
    };
    mAudioManager = (AudioManager)this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
  }

  /**
   * Executes the request.
   *
   * This method is called from the WebView thread. To do a non-trivial amount of work, use:
   *     cordova.getThreadPool().execute(runnable);
   *
   * To run on the UI thread, use:
   *     cordova.getActivity().runOnUiThread(runnable);
   *
   * @param action          The action to execute.
   * @param args            The exec() arguments.
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Whether the action was valid.
   *
   * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
   */
  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    if (action.equals(ACTION_INITIALIZE) || action.equals(ACTION_RESET_AUDIO) || action.equals(ACTION_SET_AUDIO)) {
      int volume = action.equals(ACTION_SET_AUDIO) ? args.getInt(0) : 100;
      setAudio(volume);
      if (getAudioVolume() != volume) {
        JSONObject r = new JSONObject();
        r.put("error", false);
        r.put("code", 99);
        r.put("volume", getAudioVolume());
        r.put("message", "Volume not properly set.");
        callbackContext.error(r);
        return false;
      } else {
        callbackContext.success();
      }
      return true;
    } else if (action.equals(ACTION_GET_AUDIO_VOLUME)) {
      JSONObject r = new JSONObject();
      r.put("success",true);
      r.put("volume", getAudioVolume());
      callbackContext.success(r);
      return true;
    } else if (action.equals(ACTION_SEND_EXTERNAL_DATA)) {
      return sendExternalData(args.getString(0), args.getString(1), callbackContext);
    } else if (action.equals(ACTION_GET_EXTERNAL_DATA_INTENT)) {
      return getExternalDataIntent(callbackContext);
    } else if (action.equals(ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER)) {
      return setExternalDataIntentHandler(callbackContext);
    } else if (action.equals(ACTION_REGISTER_USB_DEVICE_LISTENER)) {
      return registerUsbDeviceListener(callbackContext);
    } else if (action.equals(ACTION_UNREGISTER_USB_DEVICE_LISTENER)) {
      return unregisterUsbDeviceListener(callbackContext);
    } else if (action.equals(ACTION_GET_EXTERNAL_STORAGE_DIR)) {
      Log.d(TAG, "Executing action: " + ACTION_GET_EXTERNAL_STORAGE_DIR +  " with args: " + args.toString());
      this.openDirectoryCallbackContext = callbackContext;
      this.getExtStorageDir(args.getString(0));
      return true;
    } else if (action.equals(ACTION_COPY_EXTERNAL_STORAGE_FILES)) {
      Log.d(TAG, "Executing action: " + ACTION_COPY_EXTERNAL_STORAGE_FILES +  " with args: " + args.toString());
      this.copyExtStorageFilesCallbackContext = callbackContext;
      this.copyExtStorageFiles(args.getString(0));
      return true;
    } else if (action.equals(ACTION_DELETE_COPIED_INTERNAL_DIR)) {
      Log.d(TAG, "Executing action: " + ACTION_DELETE_COPIED_INTERNAL_DIR +  " with args: " + args.toString());
      this.deleteCopiedInternalDirCallbackContext = callbackContext;
      this.deleteCopiedInternalDir(args.getString(0));
      return true;
    }
    // } else if (action.equals(ACTION_SCAN_FLIC_BUTTON)) {
    //   return scanFlicButton(callbackContext);
    // } else if (action.equals(ACTION_DISCONNECT_FLIC_BUTTON)) {
    //   return disconnectFlicButton(args.getString(0), callbackContext);
    // } else if (action.equals(ACTION_GET_CONNECTED_FLIC_BUTTONS)) {
    //   return getConnectedFlicButtons(callbackContext);
    // } 
    else {
      callbackContext.error("Invalid action");
      return false;
    }
  }

  /**
   * Scan for and connect to a Flic2 button.
   */
  // public boolean scanFlicButton(final CallbackContext callbackContext) {
  //   if (flicCallbackContext == null) {
  //     flicCallbackContext = callbackContext;
  //   }
  //   if (isScanning) {
  //       flicManager.stopScan();
  //       isScanning = false;
  //   } else {
  //       // int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
  //       // if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
  //       //     requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
  //       //     return;
  //       // }
  //       isScanning = true;

  //       flicManager.startScan(new Flic2ScanCallback() {
  //           @Override
  //           public void onDiscoveredAlreadyPairedButton(Flic2Button button) {
  //           }

  //           @Override
  //           public void onDiscovered(String bdAddr) {
  //           }

  //           @Override
  //           public void onConnected() {
  //           }

  //           @Override
  //           public void onComplete(int result, int subCode, Flic2Button button) {
  //               isScanning = false;
  //               PluginResult pluginResult;
  //               if (result == Flic2ScanCallback.RESULT_SUCCESS) {
  //                   JSONObject r = new JSONObject();
  //                   try {
  //                       r.put("success", true);
  //                       r.put("event", "addButton");
  //                       r.put("button", button.getBdAddr());
  //                   } catch (JSONException e) {

  //                   }
  //                   pluginResult = new PluginResult(PluginResult.Status.OK, r);
  //                   pluginResult.setKeepCallback(true);
  //                   callbackContext.sendPluginResult(pluginResult);
  //                   button.addListener(new Flic2ButtonListener() {
  //                       @Override
  //                       public void onButtonUpOrDown(final Flic2Button button, boolean wasQueued, boolean lastQueued, long timestamp, boolean isUp, boolean isDown) {
  //                           if (wasQueued && button.getReadyTimestamp() - timestamp > 15000) { // This was from their sample app, but doesn't make sense to me...
  //                               // timestamp is supposedly already the time in milliseconds since this boot time, so this calculation doesn't make sense to me.
  //                               // Drop the event if it's more than 15 seconds old
  //                               return;
  //                           }
  //                           String eventValue = isDown ? "isDown" : "isUp";
  //                           JSONObject eventResponse = new JSONObject();
  //                           try {
  //                               eventResponse.put("success", true);
  //                               long ts = System.currentTimeMillis();
  //                               eventResponse.put("timestamp", ts);
  //                               eventResponse.put("event", eventValue);
  //                               eventResponse.put("button", button.getBdAddr());
  //                           } catch (JSONException e) {

  //                           }
  //                           PluginResult buttonEventResult = new PluginResult(PluginResult.Status.OK, eventResponse);
  //                           buttonEventResult.setKeepCallback(true);
  //                           flicCallbackContext.sendPluginResult(buttonEventResult);
  //                       }
  //                   });
  //               } else {
  //                   pluginResult = new PluginResult(PluginResult.Status.ERROR, "TabSINTNative.scanFlicButton error: " + Flic2Manager.errorCodeToString(result));
  //                   pluginResult.setKeepCallback(true);
  //                   callbackContext.sendPluginResult(pluginResult);
  //               }
  //           }
  //       });
  //   }
  //   return true;
  // }

  // public boolean disconnectFlicButton(String buttonId, CallbackContext callbackContext) {
  //     List<Flic2Button> allButtons = flicManager.getButtons();
  //     for (Flic2Button aButton : allButtons) {
  //         if (aButton.getBdAddr().equals(buttonId)) {
  //             flicManager.forgetButton(aButton);
  //             callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "successfully removed button: " + buttonId));
  //             return true;
  //       }
  //     }
  //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Did not disconnect, could not find button with id: " + buttonId));
  //     return false;
  // }

  // public boolean getConnectedFlicButtons(CallbackContext callbackContext) {
  //     List<Flic2Button> allButtons = flicManager.getButtons();
  //     JSONObject r = new JSONObject();
  //     JSONArray buttons = new JSONArray();
  //     for (Flic2Button aButton : allButtons) {
  //         buttons.put(aButton.getBdAddr());
  //     }
  //     try {
  //         r.put("success",true);
  //         r.put("buttons", buttons);
  //     } catch (JSONException e) {

  //     }

  //     callbackContext.success(r);
  //     return true;
  // }

  public void setAudio(int volume) {
    int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    int sysVolume = Math.round((float) (volume * (float )maxVolume / 100));
    if (sysVolume > maxVolume) {
        sysVolume = maxVolume;
    }
    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, sysVolume, 0);
  }

  public int getAudioVolume() {
    int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    int currSystemVol = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    int volLevel = Math.round((float) (currSystemVol * 100.0 / (float) maxVolume));
    return volLevel;
  }


  /**
   * Send data to another app
   * This method requires another installed app with appropriate intent filters.
   *
   * @param appName         string.  ex:  com.creare.receiver
   * @param data            string.  data = JSON.stringify(dataObject)
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Boolean regarding success
   */
  public boolean sendExternalData(final String appName, final String data, final CallbackContext callbackContext){
    if (data != null && (data instanceof String)) {
      if (appName != null && (appName instanceof String)) {
        Context activityContext = this.cordova.getActivity().getApplicationContext();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage(appName);
        sendIntent.putExtra("TABSINT_DATA_JSON_STRING", data);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activityContext.startActivity(sendIntent);
        try {
          JSONObject r = new JSONObject();
          r.put("success",true);
          r.put("message", "Receiver.sendDataExternal");
          r.put("data", data);
          callbackContext.success(r);
        } catch (JSONException e) {

        }
        return true;
      } else {
        try {
          JSONObject r = new JSONObject();
          r.put("success",false);
          r.put("message", "Could not send data to another app - no appName specified.  Name format is 'com.myorg.myapp'.");
          callbackContext.error(r);
        } catch (JSONException e) {

        }
        return false;
      }
    } else {
      try {
        JSONObject r = new JSONObject();
        r.put("success",false);
        r.put("message", "Could not send data to another app - Data must be a string.  If data is any other type, use JSON.stringify().");
        callbackContext.error(r);
      } catch (JSONException e) {

      }
      return false;
    }
  }


  /**
   * Send a JSON representation of the cordova intent back to the caller
   *
   * @param callbackContext
   */
  public boolean getExternalDataIntent (final CallbackContext callbackContext) {
      Intent intent = cordova.getActivity().getIntent();
      String action = intent.getAction();
      String type = intent.getType();
      if (Intent.ACTION_SEND.equals(action) && type != null) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getIntentJson(intent)));
        return true;
      } else {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
        return true;
      }

  }

  /**
   * Send the latest device info to the app.
  */
  private void sendUsbDeviceInfo() {
      if (mUsbDeviceCallbackContext != null) {
          mUsbConnected = mUsbDevice != null && mUsbDevice.getVendorId() == 11799 && mUsbDevice.getProductId() == 40963;
          PluginResult result;
          JSONObject usbJSON = new JSONObject();
          try {
              usbJSON.put("isUsbConnected", mUsbConnected);
              usbJSON.put("mUsbDevice", mUsbDevice != null ? mUsbDevice.getManufacturerName() : "undefined");
          }
          catch(Exception e) {
              result = new PluginResult(PluginResult.Status.ERROR, "TabSINTNative.sendUsbDeviceInfo error: " + e.getMessage());
              result.setKeepCallback(true);
              mUsbDeviceCallbackContext.sendPluginResult(result);
          }
          result = new PluginResult(PluginResult.Status.OK, usbJSON);
          result.setKeepCallback(true);
          mUsbDeviceCallbackContext.sendPluginResult(result);
      }

  }

  private boolean unregisterUsbDeviceListener(CallbackContext callbackContext) {
      if (mUsbDeviceCallbackContext != null) {
          mUsbDevice = null;
          mCordova.getActivity().getApplicationContext().unregisterReceiver(mUsbReceiver);
          mUsbDeviceCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "This context is being removed."));
          callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "Success: Removed UsbDeviceReceiver"));
          mUsbDeviceCallbackContext = null;
          return true;
      }
      callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "There is no UsbDeviceReceiver to remove."));
      return false;
  }

  /**
  * Access Documents Folder using Scoped Storage for Android 11 and API 30 upgrade (External to the Application)
  Note there is a fix for reading files internally in the app implemented in file.js
  */
  private String getFilePathFromContentUri(Uri aURI, ContentResolver contentResolver) {
    String[] req = new String[] {DocumentsContract.Document.COLUMN_DISPLAY_NAME};
    Cursor cursor = contentResolver.query(aURI, req, null, null, null);
    Log.d(TAG, "cursor: " + cursor);
    cursor.moveToFirst();
    String filePath = cursor.getString(0);
    cursor.close();
    return filePath;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
    if (resultCode == Activity.RESULT_OK) {
      Uri uri = null;
      if (resultData != null) {
        uri = resultData.getData();
        switch(requestCode) {
          case REQUEST_CODE_OPEN_DOCUMENT_TREE: {
            try {
              final ContentResolver resolver = mCordova.getActivity().getContentResolver();
              // Make permissions persistent
              final int takeFlags = resultData.getFlags()
                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
              resolver.takePersistableUriPermission(uri, takeFlags);

              // Build a response containing directory contents
              Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));
              Log.d(TAG, "onActivityResult: open directory childrenUri: " + childrenUri.toString());
              final JSONArray results = new JSONArray();
              Cursor c = null;
              String[] documentFieldsRequested = new String[] {
                  DocumentsContract.Document.COLUMN_DOCUMENT_ID, 
                  DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                  DocumentsContract.Document.COLUMN_MIME_TYPE,
                  DocumentsContract.Document.COLUMN_SIZE,
                  DocumentsContract.Document.COLUMN_LAST_MODIFIED};
              // TODO: handle 	FLAG_DIR_BLOCKS_OPEN_DOCUMENT_TREE?
              try {
                c = resolver.query(childrenUri, documentFieldsRequested, null, null, null);
                while (c.moveToNext()) {
                  //TODO: move this into its own function for getting directory entry info from a Cursor
                  final String documentId = c.getString(0);
                  final String filename = c.getString(1);
                  final String mimeType = c.getString(2);
                  final String size = c.getString(3);
                  final String lastModified = c.getString(4);
                  final boolean isDir = mimeType.equals(DocumentsContract.Document.MIME_TYPE_DIR);
                  final String fullPath = uri.toString() +'/'+ filename;
                  Log.e(TAG, "Listing files: " + filename.toString());

                  final Uri documentUri = DocumentsContract.buildDocumentUriUsingTree(uri,documentId);

                  JSONObject entryJSON = new JSONObject();
                  entryJSON.put("name", filename);
                  entryJSON.put("isDirectory", isDir);
                  entryJSON.put("isFile", !isDir);
                  entryJSON.put("fullPath", fullPath);
                  entryJSON.put("size", size);
                  entryJSON.put("lastModified", lastModified);
                
                  results.put(entryJSON);
                }  
                c.close();

                String lastModified = "";
                File directory = new File(uri.toString());

                // Uri pleaseWork = Uri.fromFile (directory);

                Date lastModifiedDate = new Date(directory.lastModified()); 
                TimeZone tz = TimeZone.getTimeZone("UTC");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
                df.setTimeZone(tz);
                String lastModifiedISO = df.format(lastModifiedDate);
 
                JSONObject path = new JSONObject();
                path.put("name", uri.getLastPathSegment());
                path.put("isDirectory", true);
                path.put("isFile", false);
                path.put("fullPath", uri.toString());
                path.put("size", "");
                path.put("lastModified", lastModifiedISO);

                JSONObject response = new JSONObject();
                response.put("success", true);
                response.put("path", path);
                // response.put("dirName", testVar);
                response.put("contents", results);

                //Send response back to Cordova JS layer
                PluginResult result = new PluginResult(PluginResult.Status.OK, response);
                result.setKeepCallback(true); // do we need this?
                this.openDirectoryCallbackContext.sendPluginResult(result);

              } catch (Exception e) {
                Log.e(TAG, "onActivityResult, LIST FILES ERROR: " + e.toString());
              } finally {
                Log.d(TAG, "onActivityResult, LIST FILES FINALLY");
              }

            }
            catch(Exception e) {
              Log.e(TAG, "onActivityResult, ERROR: " + e.toString());
            }
            finally {
              Log.d(TAG, "onActivityResult, 'try catch' finished");
              // break;
            }
          }
        }
      } else {
        Log.d(TAG, "onActivityResult: no intent included in onActivityResult call with requestCode: " + requestCode);
      }
    } else {
      Log.e(TAG, "onActivityResult: not OK...");
    }
  }

  private void openDirectory(Uri uriToLoad) {
    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
    if (uriToLoad != null) {
      intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uriToLoad);
    }
    mCordova.setActivityResultCallback(this);
    mCordova.getActivity().startActivityForResult(intent, REQUEST_CODE_OPEN_DOCUMENT_TREE);
  }

  public void getExtStorageDir(String uriString){
    Log.d(TAG, "getExtStorageDir for URI: " + uriString);
    Uri uri = Uri.parse(uriString);
    //lets check if internal directory @ protocol/local exists
    File dir = new File(mCordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/");
    Log.d(TAG, "dir:" + dir);
    if (!dir.exists()) {
      dir.mkdirs();
    }
    //proceed with selecting directory
    this.openDirectory(uri);
  }

  public void copyExtStorageFiles(String uriString){
    Log.d(TAG, "copyExtStorageFiles for URI: " + uriString);
    Uri uri = Uri.parse(uriString);
    final ContentResolver resolver = mCordova.getActivity().getContentResolver();

    //create internal directory to copy files to
    String nameToParse = uri.getLastPathSegment().toString();
    //the paths can look different, can have ':' or '/', dont include those in the actual name
    String[] splitName0 = nameToParse.split("/");
    String dirName0 = splitName0[splitName0.length - 1];
    String[] splitName = dirName0.split(":");
    String dirName = splitName[splitName.length - 1];
    File dir = new File(mCordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/"+dirName);
    Log.d(TAG, "dir:" + dir);
    if (!dir.exists()) {
      dir.mkdirs();
    }
    //delete contents of the directory
    this.clearDir(dir);

    //create childrenUri to loop through all files and copy each one
    Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));

    Cursor c = null;
    String[] documentFieldsRequested = new String[] {
      DocumentsContract.Document.COLUMN_DOCUMENT_ID, 
      DocumentsContract.Document.COLUMN_DISPLAY_NAME
    };
    c = resolver.query(childrenUri, documentFieldsRequested, null, null, null);
    while (c.moveToNext()) {
      final String documentId = c.getString(0);
      final String filename = c.getString(1);
      final Uri documentUri = DocumentsContract.buildDocumentUriUsingTree(uri,documentId);
      Log.d(TAG, "copyExtStorageFiles for documentURI: " + documentUri);

      try {
        FileInputStream original_loc = new FileInputStream(mCordova.getActivity().getApplicationContext().getContentResolver().openFileDescriptor(documentUri, "r").getFileDescriptor());

        File copy_loc_file = new File(mCordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/" + dirName + "/" + filename);
        if (!copy_loc_file.exists()) {
          copy_loc_file.createNewFile();
        }
        Log.d(TAG, "copy_loc_file:" + copy_loc_file);

        FileOutputStream copy_loc = new FileOutputStream(copy_loc_file);
        Log.d(TAG, "copy_loc:" + copy_loc);
        
        this.copyFileUsingStream(documentUri, copy_loc_file);

      }
      catch (Exception e) {
        Log.e(TAG, "ERROR in copyExtStorageFiles: " + e.toString());
      }
      finally {
        Log.e(TAG, "finally inside loop in copyExtStorageFiles");
      }

    }
    c.close();
    //after loop
    try {
      Log.e(TAG, "We never seem to make it this far...");
      JSONObject response = new JSONObject();
      response.put("success", true);
      PluginResult result = new PluginResult(PluginResult.Status.OK, response);
      result.setKeepCallback(true); // do we need this?
      this.copyExtStorageFilesCallbackContext.sendPluginResult(result);
    }
    catch (Exception e) {
      Log.e(TAG, "ERROR in copyExtStorageFiles creating JSON: " + e.toString());
    }
    finally {
      Log.e(TAG, "Last finally in copyExtStorageFiles");
    }
  }

  private void copyFileUsingStream(Uri source, File dest) throws IOException {
    InputStream is = null;
    OutputStream os = null;
    try {
      is = new FileInputStream(mCordova.getActivity().getApplicationContext().getContentResolver().openFileDescriptor(source, "r").getFileDescriptor());
      os = new FileOutputStream(dest);
      byte[] buffer = new byte[1024];
      int length;
      while ((length = is.read(buffer)) > 0) {
          os.write(buffer, 0, length);
      }
    } 
    finally {
      is.close();
      os.close();
    }
  }

  public void deleteCopiedInternalDir(String uriString) {
    Log.d(TAG, "Deleting Directory @ URI: " + uriString);
    Uri uri = Uri.parse(uriString);
    final ContentResolver resolver = mCordova.getActivity().getContentResolver();
    //delete contents of the directory
    String nameToParse = uri.getLastPathSegment().toString();
    String[] splitName0 = nameToParse.split("/");
    String dirName0 = splitName0[splitName0.length - 1];
    String[] splitName = dirName0.split(":");
    String dirName = splitName[splitName.length - 1];
    File dir = new File(mCordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/"+dirName);
    this.clearDir(dir);
    //now, delete the directory
    dir.delete();
    Log.d(TAG, "Directory Deleted?  : " + uriString);
    //send response back
    try {
      JSONObject response = new JSONObject();
      response.put("success", true);
      PluginResult result = new PluginResult(PluginResult.Status.OK, response);
      result.setKeepCallback(true); // do we need this?
      this.deleteCopiedInternalDirCallbackContext.sendPluginResult(result);
    }
    catch (Exception e) {
      Log.e(TAG, "ERROR in deleteCopiedInternalDir creating JSON: " + e.toString());
    }
    finally {
      Log.e(TAG, "Last finally in deleteCopiedInternalDir");
    }
  }

  public void clearDir(File clr) {
    File[] files = clr.listFiles();
    if (files == null) {
      return;
    }
    for (File f : files) {
      if (f.isFile()) {
        f.delete();
        Log.e(TAG, "Deleted File: " + f.toString());
      }
    }
  }

  private boolean registerUsbDeviceListener(final CallbackContext callbackContext) {
      if (mUsbDeviceCallbackContext == null) {
          mUsbDeviceCallbackContext = callbackContext;
          mCordova.getActivity().getApplicationContext().registerReceiver(mUsbReceiver, mFilter);

          // Check for current USB device
          mUsbDevice = null;
          HashMap<String, UsbDevice> usbDevices = mUsbManager.getDeviceList();
          if (usbDevices.values().size() > 0) {
              for (UsbDevice d : usbDevices.values()) {
                  mUsbDevice = d;
                  break;
              }
          }
          sendUsbDeviceInfo();
          return true;
      }
      mUsbDeviceCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "UsbDeviceReceiver already registered."));
      return false;

  }

  /**
   * Register handler for onNewIntent event
   *
   * @param data
   * @param callbackContext
   * @return
   */
  public boolean setExternalDataIntentHandler (final CallbackContext callbackContext) {
      this.onNewIntentCallbackContext = callbackContext;

      PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
      result.setKeepCallback(true);
      callbackContext.sendPluginResult(result);

      return true;
  }

  /**
   * Triggered on new intent
   *
   * @param intent
   */
  @Override
  public void onNewIntent(Intent intent) {
      PluginResult pluginResult;
      String action = intent.getAction();
      String type = intent.getType();
      if (this.onNewIntentCallbackContext != null) {
          if (Intent.ACTION_SEND.equals(action) && type != null) {
            pluginResult = new PluginResult(PluginResult.Status.OK, getIntentJson(intent));
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          } else {
            pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          }
      }
  }

  /**
   * Return JSON representation of intent attributes
   *
   * @param intent
   * @return
   */
  private JSONObject getIntentJson(Intent intent) {
      JSONObject intentJSON = null;

      try {
          intentJSON = new JSONObject();

          intentJSON.put("type", intent.getType());
          intentJSON.put("data", intent.getStringExtra("TABSINT_DATA_JSON_STRING"));

          return intentJSON;
      } catch (JSONException e) {
          Log.d(TAG, TAG + " Error thrown during intent > JSON conversion");
          Log.d(TAG, e.getMessage());
          Log.d(TAG, Arrays.toString(e.getStackTrace()));

          return null;
      }
  }

}