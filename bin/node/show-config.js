#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var process = require('process');

if (require.main === module) {
  var c = config.read();
  log.info('Configuration File: \n\n' + JSON.stringify(c, null, '  '));
}
