#!/usr/bin/env node

/**
 * Run commands on the shell
 * @module
 */
var cmd = module.exports = {};

var log = require('./log');
var process = require('process');
var execSync = require('child_process').execSync;

/**
 * Synchronously run a command on the shell
 * @param  {string} command - command to run on the shell
 * @param  {boolean} force - continue process even if command fails
 *
 * @return {object}         Object with two keys: 'error', which is the error object returned from the command, and 'stderr' which is the stderr as a string
 */
cmd.run = function(command, force) {

  try {

    log.debug('Running command: ' + command);
    var ret = execSync(command, { stdio: [null, null, null] });
    // log.debug('Command returned: ' + ret);
    return ret.toString();

  } catch(e) {

    var stack = e.stack || 'stack undefined';
    var err = e.error || e.Error || {};
    var stderr = e.stderr ? e.stderr.toString() : 'stderr undefined';

    var ret = {
      stack: stack,
      error: err,
      stderr: stderr
    };

    if (force) {
      log.warn('Error while running command: ' + command + ' with stderr: ' + ret.stderr);
      return ret;
    } else {
      log.error('Error while running command: ' + command);
      log.error('Node error stack: \n' + stack);
      process.exit(1);
    }

  }

};

