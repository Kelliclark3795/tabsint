%% Calculate average number of presentations to convergence 
% for Hughson Westlake Results
% Plot threshold versus number of presentations to convergence

% Load Data
% All result json files of interest must be in the current directory

% Create the results structure
tr = TabsintResults;

% Load raw data and all exam results
tr.loadresults();       

% Load responses from Hughson Westlake response areas
audiometry = tr.loadaudiometry(); 
                    
% Find the number of presentations it takes to reach convergence
% by measuring the length of the presentation levels.
convergence = zeros(1,length(audiometry));
for i=1:length(convergence)
    convergence(i) = length(audiometry(i).L);
end

% calculate convergence statistics
avg_convergence = mean(convergence);
min_time = 1.775*avg_convergence;
max_time = 2.175*avg_convergence;

% Get the index of non-empty thresholds
notEmpty = find(~cellfun(@isempty,{audiometry.Threshold}));

% Plot thresold vs. number of presentations until convergence
figure();
scatter(convergence(notEmpty),[audiometry.Threshold],'filled');
xlabel('Number of Presentations');
ylabel('Threshold');
title('Threshold vs. Number of Presentations');

clear i notEmpty