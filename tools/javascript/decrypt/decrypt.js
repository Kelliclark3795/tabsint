/**
 * Results Decryptor using JSEncrypt
 *
 * HG
 *
 * Requires:
 *
 * - node-jsencrypt
 * - crypto-js
 *
 * Usage:
 * node decrypt <private-key-path> <encrypted-result-key-path> <encrypted-result-path> <output-filename> <node_modules-path>
 *
 * Example:
 * node decrypt example-key.pem example-result.json.key.enc example-result.json.enc decrypted-result.json ../../../node_modules/
 */
var args = process.argv.slice(2);

var privateKeyFile = args[0];
var JSONKeyFile = args[1];
var JSONEncFile = args[2];
var JSONDataFile = args[3];
var nodeDir = args[4];

var fs = require("fs");
var CryptoJS = require(nodeDir + "crypto-js");
var nodeJsencrypt = require(nodeDir + "node-jsencrypt");
var privateKey = fs.readFileSync(privateKeyFile, "utf8");
var encryptedAESKey = fs.readFileSync(JSONKeyFile, "utf8");
var decrypt = new nodeJsencrypt();
decrypt.setPrivateKey(privateKey);
var key256Bits = decrypt.decrypt(encryptedAESKey);
var encodeData = fs.readFileSync(JSONEncFile, "utf8");
var bytes = CryptoJS.AES.decrypt(encodeData, key256Bits);
var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
fs.writeFileSync(JSONDataFile, JSON.stringify(decryptedData));
console.log("SUCCESS");
