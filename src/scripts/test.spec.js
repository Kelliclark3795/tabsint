import * as angular from "angular";
import "angular-mocks";

// add all tests here by path
import "./services/admin/admin.spec";
import "./services/advanced-protocol/advanced-protocol.spec";
import "./services/cha-results/cha-results.spec";
import "./services/config/config.spec";
import "./services/cordova/tabsintNative/tabsintNative.spec";
import "./services/cordova/media/media.spec";
import "./services/csv/csv.spec";
import "./services/disk/disk.spec";
import "./services/gain/gain.spec";
import "./services/router/router.spec";
import "./services/svantek/svantek.spec";

// // components
import "./components/response-areas/multiple-choice/multiple-choice.spec";
import "./components/response-areas/checkbox/checkbox.spec";
import "./components/response-areas/audiometry-input/audiometry-input.spec";

// commented out have the error "cordova is not defined"
// import "./app.spec";
// import "./services/cha/cha.spec";
// import "./services/exam/exam.spec";
// import "./services/gitlab/gitlab.spec";
// import "./services/logger/logger.spec";
// import "./services/protocol/protocol.spec";
// import "./services/results/results.spec";
// import "./services/tabsint-server/tabsint-server.spec";
// import "./components/response-areas/response-areas.spec";
// import "./components/response-areas/audiometry/audiogram/audiogram.spec";
