/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./protocols/protocols";
import "./results/results";
import "./config/config";

angular
  .module("tabsint.routes.admin", [
    "tabsint.routes.admin.results",
    "tabsint.routes.admin.protocols",
    "tabsint.routes.admin.config"
  ])

  .directive("adminView", function() {
    return {
      restrict: "E",
      controller: "AdminCtrl",
      templateUrl: "scripts/routes/admin/admin.html",
      scope: {}
    };
  })

  .controller("AdminCtrl", function($scope, adminLogic, examLogic) {
    $scope.admin = adminLogic;
    $scope.examLogic = examLogic;
  });
