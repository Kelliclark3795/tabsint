"use strict";

import * as angular from "angular";
import "angular-mocks";
import "./app";

beforeEach(angular.mock.module("tabsint"));
beforeEach(function() {
  localStorage.clear();
});

describe("App test", function() {
  // load the controller's module

  beforeEach(
    angular.mock.inject(function(_$httpBackend_) {
      _$httpBackend_.whenGET("res/translations/translations.json").respond({});
    })
  );

  var TabsintCtrl, scope, app;

  // Initialize the controller and a mock scope
  beforeEach(
    angular.mock.inject(function($controller, $rootScope) {
      $rootScope.$digest(); // necessary to get app.run to fire all the way through

      //load new scope and then load controller
      scope = $rootScope.$new();
      $controller("TabsintCtrl", {
        $scope: scope
      });
    })
  );

  // beforeEach(
  //   angular.mock.inject(function(_$window_) {
  //     $window = _$window_;
  //   })
  // );

  // $window.resolveLocalFileSystemURL = function() {
  //   return {};
  // };

  // spyOn($window, "resolveLocalFileSystemURL").and.returnValue($window.resolveLocalFileSystemURL);

  it("should have the router on its scope", function() {
    expect(scope.router).toBeDefined();
  });

  it("should be on the welcome page", function() {
    expect(scope.router.page).toEqual("WELCOME");
  });
});
