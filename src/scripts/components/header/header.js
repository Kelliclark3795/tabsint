/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as LZString from "lz-string";

angular
  .module("tabsint.components.header", [])

  .directive("header", function() {
    return {
      templateUrl: "scripts/components/header/header.html",
      transclude: true,
      scope: {
        isexam: "=",
        build: "="
      },
      controller: function(
        $q,
        $scope,
        adminLogic,
        app,
        authorize,
        autoConfig,
        config,
        disk,
        examLogic,
        gettextCatalog,
        logger,
        notifications,
        page,
        router,
        tasks
      ) {
        $scope.router = router;
        $scope.dm = examLogic.dm;
        $scope.page = page;
        $scope.tasks = tasks;
        $scope.disk = disk;
        $scope.app = app;

        $scope.resetExam = function() {
          notifications.confirm(
            gettextCatalog.getString("Reset Exam and Discard Partial Results?"),
            function(buttonIndex) {
              if (buttonIndex === 1) {
                authorize.modalAuthorize(function() {
                  examLogic.reset();
                  disk.currentResults = undefined; // can reset this now - we have generated a proper result
                }, disk.debugMode);
              } else if (buttonIndex === 2) {
                logger.info("Reset Canceled");
              }
            },
            gettextCatalog.getString("Confirm Selection"),
            [gettextCatalog.getString("OK"), gettextCatalog.getString("Cancel")]
          );
        };

        $scope.submitPartialExam = function() {
          notifications.confirm(
            gettextCatalog.getString("End Exam and Submit Partial Results?"),
            function(buttonIndex) {
              if (buttonIndex === 1) {
                authorize.modalAuthorize(function() {
                  examLogic.submitPartial();
                }, disk.debugMode);
              } else if (buttonIndex === 2) {
                logger.info("submitPartial Canceled");
              }
            },
            gettextCatalog.getString("Confirm Selection"),
            [gettextCatalog.getString("OK"), gettextCatalog.getString("Cancel")]
          );
        };

        $scope.navigateToTarget = function(navOption) {
          var confirmText = navOption.text + "?  ";
          if (!!navOption.returnHereAfterward) {
            confirmText += gettextCatalog.getString(
              "TabSINT will navigate to the selected sub-protocol, then return to this page and resume the current series of questions after that sub-protocol is complete."
            );
          } else {
            confirmText += gettextCatalog.getString(
              "Results from this page will be lost and the current series of questions will be aborted."
            );
          }

          notifications.confirm(
            confirmText,
            function(buttonIndex) {
              if (buttonIndex === 1) {
                authorize.modalAuthorize(function() {
                  examLogic.navigateToTarget(navOption);
                }, disk.debugMode);
              } else if (buttonIndex === 2) {
                logger.info("navigateToTarget Canceled");
              }
            },
            gettextCatalog.getString("Confirm Selection"),
            [gettextCatalog.getString("OK"), gettextCatalog.getString("Cancel")]
          );
        };

        $scope.switchToExamView = function() {
          examLogic.switchToExamView();
        };

        $scope.switchToAdminView = function() {
          adminLogic.switchToAdminView();
        };

        /**
         * Scan Config Qr code and load it into the config object
         */
        $scope.scanQrCodeandAutoConfig = function() {
          function scanQrCode() {
            var deferred = $q.defer();
            var scanner;
            // check that QR code scanner is enabled
            try {
              scanner = cordova.plugins.barcodeScanner;
            } catch (e) {
              logger.error("Could not load barcode scanner: " + angular.toJson(e, true));
              deferred.reject("Could not load barcode scanner with error: " + e);
            }
            scanner.scan(
              function(result) {
                let decompressed;

                // try parsing with JSON first for backwards compatibility
                // if this fails, parse with LZString
                try {
                  let c = JSON.parse(result.text);
                  if (typeof c !== "object") {
                    throw "Failed to parse text to object";
                  }
                  decompressed = result.text;
                } catch (e) {
                  decompressed = LZString.decompressFromBase64(result.text);
                }

                logger.debug("QR Scan: " + decompressed);

                $scope.$apply(function() {
                  if (result.cancelled) {
                    deferred.reject("Scanning cancelled");
                  }

                  // confirm we can read object
                  try {
                    let c = JSON.parse(decompressed);
                    if (typeof c !== "object") {
                      deferred.reject("Failed to read configuration");
                    }
                  } catch (e) {
                    deferred.reject("Failed to parse configuration text");
                  }

                  deferred.resolve(decompressed);
                });
              },
              function(result) {
                $scope.$apply(function() {
                  deferred.reject("Scanner failed with error: " + angular.toJson(result, true));
                });
              },
              {
                orientation: "landscape",
                formats: "QR_CODE, PDF_417, CODE_128", // default: all but PDF_417 and RSS_EXPANDED
                disableSuccessBeep: true
              }
            );
            return deferred.promise;
          }

          return scanQrCode()
            .catch(function(e) {
              if (e !== "Scanning cancelled") {
                notifications.alert(
                  gettextCatalog.getString("TabSINT failed to read configuration code error: ") +
                    angular.toJson(e, true)
                );
              }

              return $q.reject();
            })
            .then(function(configObject) {
              return config.load(configObject, true);
            }) // Load the configuration Qr Code into the config object
            .then(autoConfig.load); // Act on the config object: apply admin settings, load protocol and exam view
        };
      }
    };
  });
