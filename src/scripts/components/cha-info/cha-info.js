/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.components.cha.info", []).component("chaInfo", {
  templateUrl: "scripts/components/cha-info/cha-info.template.html",
  controller: [
    "$scope",
    "cha",
    "$sce",
    "gettextCatalog",
    "changeTimer",
    "disk",
    function ChaInfoController($scope, cha, $sce, gettextCatalog, changeTimer, disk) {
      var self = this;
      self.cha = cha;
      $scope.cha = cha;
      $scope.disk = disk;
      $scope.changeTimer = changeTimer;
      $scope.setShutdownTimerPopover = $sce.trustAsHtml(
        gettextCatalog.getString("Auto shutdown time (in minutes) for the WAHTS headset.")
      );
    }
  ]
});
