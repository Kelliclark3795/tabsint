/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.cha.connection", [])

  .directive("chaConnection", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/cha-connection/cha-connection.html",
      controller: "ChaConnectionCtrl",
      scope: {}
    };
  })
  .controller("ChaConnectionCtrl", function($scope, cha, disk) {
    $scope.cha = cha;
    $scope.debugMode = disk.debugMode;
    $scope.myCha = disk.cha.myCha;
  });
