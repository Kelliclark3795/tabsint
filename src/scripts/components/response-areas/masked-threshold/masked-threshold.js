/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";
import "../audiometry/audiometry-properties/audiometry-properties";

angular
  .module("tabsint.components.response-areas.cha.masked-threshold", ["cha.audiometry-properties"])
  .directive("maskedThresholdExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/masked-threshold/masked-threshold.html",
      controller: "MaskedThresholdExamCtrl"
    };
  })
  .controller("MaskedThresholdExamCtrl", function(
    $scope,
    cha,
    chaExams,
    examLogic,
    logger,
    page,
    results,
    chaAudiometryService
  ) {
    $scope.chaExams = chaExams;
    chaAudiometryService.reset();

    chaAudiometryService.examDone = false;
    // wait until exam is over
    chaExams.wait.forReadyState().then(function() {
      chaAudiometryService.examDone = true;
      cha
        .requestResults()
        .then(function(resultFromCha) {
          page.result = $.extend({}, page.result, resultFromCha);
          page.result.examProperties = chaExams.examProperties;
          if (angular.isUndefined(page.result.examProperties.OutputChannel)) {
            // Per spec, specifying OutputChannel is optional in protocol and defaults to
            // 'HPL0' if unspecified
            page.result.examProperties.OutputChannel = "HPL0";
          }
          page.result.examType = chaExams.examType;
          page.result.MaskingLevel = resultFromCha.ML[resultFromCha.ML.length - 1];

          page.dm.isSubmittable = true;

          if (page.dm.responseArea.autoSubmit) {
            examLogic.submit();
          }
        })
        .catch(function(err) {
          if (err.msg !== "repeating exam") {
            cha.errorHandler.main(err);
          }
        });
    });
  });
