/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../app";

beforeEach(angular.mock.module("tabsint"));

var page;

describe("Response Area: Multiple Choice", function() {
  var parentCtrl, childCtrl, parentScope, childScope, page, myChoice;

  beforeEach(
    angular.mock.inject(function($controller, $rootScope, _page_) {
      _page_.dm = {
        isSubmittable: false,
        responseArea: {
          choices: [{ id: "A", correct: true }, { id: "B" }, { id: "C" }, { id: "D" }],
          feedback: "showCorrect"
        }
      };
      _page_.result = {
        response: undefined,
        otherResponse: undefined
      };

      page = _page_;

      parentScope = $rootScope.$new(); // start a new scope
      childScope = $rootScope.$new(); // start a new scope

      myChoice = page.dm.responseArea.choices[1];

      childScope.choice = myChoice;

      parentCtrl = $controller("MultipleChoiceResponseAreaCtrl", {
        $scope: parentScope
      });

      childCtrl = $controller("BasicChoiceController", {
        $scope: childScope
      });
    })
  );

  it("should have default options defined", function() {
    expect(parentScope.enableOther).toBe(false);
  });

  it("should have the selected choice propagate to the response", function() {
    childScope.choose();

    expect(page.result.response).toBe(myChoice.id);
  });

  it("should handle feedback properly", function() {
    page.dm.showFeedback();

    expect(parentScope.showCorrect).toBe(true);

    page.dm.responseArea.feedback = "gradeResponse";

    page.dm.showFeedback();

    expect(parentScope.gradeResponse).toBe(true);
  });
});
