/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.dichotic-digits", [])
  .directive("dichoticDigitsExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/dichotic-digits/dichotic-digits.html",
      controller: "DichoticDigitsExamCtrl"
    };
  })
  .controller("DichoticDigitsExamCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    examLogic,
    logger,
    notifications,
    page,
    results
  ) {
    // Function to run at exam initiation - happens when the responseArea changes
    function newDichoticDigitsExam() {
      $scope.digitsDisabled = true;
      $scope.readyToProcess = false; // activated when the presentation info is set in "presentationHandler"
      $scope.presentationCount = 0; // counted number of presentation displayed
      if (_.isUndefined(chaExams.examProperties.NumberOfPresentations)) {
        chaExams.examProperties.NumberOfPresentations = 20;
      }

      page.dm.isSubmittable = false; // make the page not submittable

      resetResults();
      getPresentationInfo();
    }

    function getPresentationInfo() {
      return cha
        .requestResults()
        .then(function(result) {
          presentationHandler(result);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    // This is less a results handler in this case, more an exam handler
    function presentationHandler(resultFromCha) {
      if (resultFromCha.State === 0 || resultFromCha.State === 1) {
        // State 0 = exam still running, // State 1 =  waiting for result
        // Put presentation properties in the result
        page.result = $.extend({}, page.result, {
          presentationCount: $scope.presentationCount,
          PresentedFile: resultFromCha.PresentedFile,
          PresentedDigits: resultFromCha.PresentedDigits
        });
        $scope.readyToProcess = true;
      } else if (resultFromCha.State === 2) {
        // State 2 = End of exam
        page.dm.isSubmittable = true; // make the page submittable

        page.result = $.extend({}, results.default(page.dm), page.result, resultFromCha, {
          response: "Exam Results",
          chaInfo: chaExams.getChaInfo()
        });
        examLogic.submit();
      }
    }

    // grade the user inputs and show correct answers
    function processDigits() {
      if ($scope.readyToProcess) {
        cha
          .requestResults()
          .then(function(result) {
            if (result.State === 0) {
              // State 0 = exam still running, ask again in 200 ms
              $timeout(processDigits, 500);
            } else if (result.State === 1) {
              // State 1 =  waiting for result
              logger.info("CHA processing entered digits: " + $scope.response);
              $scope.digitsDisabled = true;
              $scope.readyToProcess = false;
              $scope.feedback = angular.isDefined(page.dm.responseArea.feedback) ? page.dm.responseArea.feedback : true;
              let presDigits = _.cloneDeep(page.result.PresentedDigits);
              let presentationScore = 0;
              _.forEach($scope.response, function(digit, index) {
                if (presDigits.includes(digit)) {
                  $scope.digitCorrect[index] = true;
                  presDigits.splice(presDigits.indexOf(digit), 1);
                  presentationScore += 25;
                }
              });
              page.result.presentationScore = presentationScore;
              var feedbackDelay = page.dm.responseArea.feedbackDelay || 1000;
              $timeout(submitDigits, feedbackDelay); //  Allow the correct answers to show for ~1 second before submitting results.
            }
          })
          .catch(function(err) {
            cha.errorHandler.main(err);
          });
      } else {
        $timeout(processDigits, 500);
      }
    }

    // Send digits back to the CHA and push presentation score onto the exam results stack
    function submitDigits() {
      page.result.response = $scope.response;
      // push results
      examLogic.pushResults();

      // send results to cha and start new presentation
      var submission = { CorrectDigits: page.result.response };
      logger.debug("Submitting digits to the CHAs: " + JSON.stringify(submission));
      return (
        cha
          .examSubmission("DichoticDigits$Submission", {
            CorrectDigits: page.result.response
          })
          //          .then(chaExams.runTOBifCalledFor)
          .then(resetResults)
          .then(getPresentationInfo)
          .catch(function(err) {
            cha.errorHandler.main(err);
          })
      );
    }

    function resetResults() {
      $scope.resetKeypad(); // reset keypad
      $scope.feedback = false; //reset feedback
      $scope.digitCorrect = [false, false, false, false]; //reset digit answers

      // activate keypad
      var keypadDelay = page.dm.responseArea.keypadDelay || 5;
      $timeout(activateKeypad, keypadDelay); // Activate keypad after N second

      var presentationId = page.result.presentationId;
      // TODO: this should be replaced by a "page.newResult()" or "results.new()"
      // this will require a change to examlogic, so skipping for now
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        presentationId: presentationId,
        responseStartTime: new Date()
      };
    }

    function activateKeypad() {
      $scope.digitsDisabled = false;
      $scope.presentationCount += 1;
    }

    // Clear keypad at the start and after each response - also fires on 'Clear' keypress
    $scope.resetKeypad = function() {
      $scope.response = [];
      $scope.digitCorrect = [false, false, false, false];
      $scope.feedback = false;
    };

    // Delete last digit from keypad
    $scope.deleteKeypad = function() {
      $scope.response = $scope.response.slice(0, -1);
      $scope.feedback = false;
    };

    // User input from HTML
    $scope.addDigit = function(digit) {
      if ($scope.response.length < 4) {
        $scope.response.push(digit);
        if ($scope.response.length === 4) {
          processDigits();
        }
      }
    };

    // start new Dichotic Digits Exam
    newDichoticDigitsExam();
  });
