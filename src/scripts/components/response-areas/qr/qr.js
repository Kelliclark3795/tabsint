/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.qr", [])

  .controller("QrResponseAreaCtrl", function($scope, examLogic, page, results, notifications, logger, gettextCatalog) {
    // defaults
    page.dm.responseArea.scope = page.dm.responseArea.scope || "exam"; // default property for scope
    page.dm.responseArea.autoSubmit = angular.isDefined(page.dm.responseArea.autoSubmit)
      ? page.dm.responseArea.autoSubmit
      : true; // default property for autoSubmit
    page.dm.submitText = "Submit";

    // backwards compatibility with `required`
    if (angular.isDefined(page.dm.responseArea.required)) {
      page.dm.responseArea.responseRequired = page.dm.responseArea.required;
    }

    // define initial submittable logic
    page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);

    var scanner;
    try {
      scanner = cordova.plugins.barcodeScanner;
      $scope.qrCodeEnabled = true;
    } catch (err) {
      $scope.qrCodeEnabled = false;
      logger.error("Could not load barcode scanner: " + angular.toJson(err, true));
    }

    $scope.scanQrCode = function() {
      // check that QR code scanner is enabled
      if (!$scope.qrCodeEnabled) {
        logger.warn("QR Scanning not enabled");
        return;
      }

      scanner.scan(
        function(result) {
          logger.debug("QR Scan: " + angular.toJson(result, true));

          $scope.$apply(function() {
            if (result.cancelled) {
              return;
            }

            // show qrcode in view
            $scope.qrResults = result.text;

            // save page response
            page.result.response = result.text;

            // save on top level of exam result
            if (page.dm.responseArea.scope === "exam") {
              results.current.qrString = result.text;
            }

            // make page submittable
            page.dm.isSubmittable = true;

            // auto-submit if enabled
            if (page.dm.responseArea.autoSubmit) {
              examLogic.submit();
            }
          });
        },
        function(result) {
          notifications.alert(
            gettextCatalog.getString("TabSINT failed to scan QR Code with error: ") + angular.toJson(result, true)
          );
          $scope.digest();
        },
        {
          orientation: "landscape",
          formats: "QR_CODE, PDF_417, CODE_128", // default: all but PDF_417 and RSS_EXPANDED
          disableSuccessBeep: true
        }
      );
    };
  });
