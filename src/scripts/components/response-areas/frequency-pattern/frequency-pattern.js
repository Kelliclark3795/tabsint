/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.frequency-pattern", [])

  .directive("frequencyPatternExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/frequency-pattern/frequency-pattern.html",
      controller: "FrequencyPatternExamCtrl"
    };
  })

  .controller("FrequencyPatternExamCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    examLogic,
    logger,
    page,
    results
  ) {
    // Initialize variables
    var pollingTimeout, currentState;
    $scope.chaExams = chaExams;
    $scope.buttonsDisabled = true; // Play each presentation in full with no disabled, then enabled buttons asking listener if they heard tones.
    $scope.patternEntered = [];
    $scope.presentationCount = 0;
    $scope.nCorrect = 0;
    $scope.imagePath1 = "img/frequency-pattern-blank.png";
    $scope.imagePath2 = "img/frequency-pattern-blank.png";
    $scope.imagePath3 = "img/frequency-pattern-blank.png";
    // 0:incorrect; 1:correct; "":reset
    $scope.fpSelection1 = "";
    $scope.fpSelection2 = "";
    $scope.fpSelection3 = "";
    var feedbackDelay = angular.isDefined(page.dm.responseArea.feedbackDelay)
      ? page.dm.responseArea.feedbackDelay
      : 1000;
    var feedback = angular.isDefined(page.dm.responseArea.feedback) ? page.dm.responseArea.feedback : true;
    var prevPresentedPattern; // Need to keep track of previous presented pattern, because that is what needs to be compared to the current response for feedback

    page.dm.isSubmittable = false; // make the page not submittable

    // onExit function, to make sure intervals do not remain
    $scope.$on("$destroy", function() {
      $timeout.cancel(pollingTimeout);
    });

    // exam initiation, happens when response area is loaded
    resetResults();
    getState();

    // start polling the cha for state
    function getState() {
      return cha
        .requestResults()
        .then(handleState)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    // handle state response from cha
    function handleState(resultFromCha) {
      currentState = resultFromCha.State;
      if (currentState === 0) {
        // State 0 = exam still running, ask again in 200 ms
        // enable response
        $scope.buttonsDisabled = false;
        pollingTimeout = $timeout(function() {
          getState();
        }, 200);
      } else if (currentState === 1) {
        prevPresentedPattern = resultFromCha.PresentedPattern;
        page.result = $.extend({}, page.result, results.default(page.dm), resultFromCha);
        delete page.result.Score; // Score is only valid when state=2
        delete page.result.NumberOfReversals; // NumberOfReversals is only valid when state=2
        // State 1 =  waiting for result
        // enable response
        $scope.buttonsDisabled = false;
        // Wait for user input
      } else if (currentState === 2) {
        // State 2 =  exam done
        // disable response
        $scope.buttonsDisabled = true;
        // End of exam
        delete page.result.presentationCount; // presentationCount irrelevent above NumberOfPresentations
        page.result = $.extend({}, page.result, resultFromCha, {
          response: "Exam Results",
          chaInfo: chaExams.getChaInfo()
        });
        // Ready to submit
        page.dm.isSubmittable = true;

        examLogic.submit();
      }
    }

    //adds red/green border around image once all selections are made
    function checkChosenSelections() {
      // are answers correct?
      var blnfpSelection1 = $scope.patternEntered[0] === prevPresentedPattern.toString()[0] ? true : false;
      var blnfpSelection2 = $scope.patternEntered[1] === prevPresentedPattern.toString()[1] ? true : false;
      var blnfpSelection3 = $scope.patternEntered[2] === prevPresentedPattern.toString()[2] ? true : false;
      // set (in)correct view
      $scope.fpSelection1 = blnfpSelection1 ? "1" : "0";
      $scope.fpSelection2 = blnfpSelection2 ? "1" : "0";
      $scope.fpSelection3 = blnfpSelection3 ? "1" : "0";
    }
    //resets border around selections
    function clearChosenSelections() {
      $scope.fpSelection1 = "";
      $scope.fpSelection2 = "";
      $scope.fpSelection3 = "";
    }

    // Submit user response to the CHA
    function submitUserInput() {
      let patternEntered = parseInt($scope.patternEntered);
      examLogic.pushResults(); // Submit presention to TabSINT

      // submit result
      cha
        .examSubmission("FrequencyPattern$Submission", { EnteredPattern: patternEntered })
        .then(cha.requestResults)
        .then(function(resultFromCha) {
          if (resultFromCha.Correct) {
            $scope.nCorrect += 1;
          }
        })
        .then(resetResults)
        .then(getState)
        .catch(function(err) {
          $timeout.cancel(pollingTimeout);
          cha.errorHandler.main(err);
        });
    }

    // Check that the exam is waiting for results before submitting exam
    function checkChaStatus() {
      if (currentState === 0) {
        // State 0 = exam still running, ask again in 200 ms
        pollingTimeout = $timeout(function() {
          checkChaStatus();
        }, 200);
      } else if (currentState === 1) {
        // State 1 =  waiting for result
        // Submit user input
        if (feedback) {
          checkChosenSelections();
        }
        $timeout(submitUserInput, feedbackDelay); //  Allow the correct answers to show for feedbackDelay milliseconds before submitting results.
      }
    }

    // Add image and user input when user selects an input
    $scope.addResponse = function(userResponse) {
      if ($scope.patternEntered.length < 3) {
        $scope.patternEntered += userResponse;
        if ($scope.patternEntered.length === 1) {
          if (userResponse === 0) {
            $scope.imagePath1 = "img/frequency-pattern-unknown.png";
          } else if (userResponse === 1) {
            $scope.imagePath1 = "img/frequency-pattern-frog.png";
          } else if (userResponse === 2) {
            $scope.imagePath1 = "img/frequency-pattern-bird.png";
          }
        }
        if ($scope.patternEntered.length === 2) {
          if (userResponse === 0) {
            $scope.imagePath2 = "img/frequency-pattern-unknown.png";
          } else if (userResponse === 1) {
            $scope.imagePath2 = "img/frequency-pattern-frog.png";
          } else if (userResponse === 2) {
            $scope.imagePath2 = "img/frequency-pattern-bird.png";
          }
        }
        if ($scope.patternEntered.length === 3) {
          // disable response
          $scope.buttonsDisabled = true;
          if (userResponse === 0) {
            $scope.imagePath3 = "img/frequency-pattern-unknown.png";
          } else if (userResponse === 1) {
            $scope.imagePath3 = "img/frequency-pattern-frog.png";
          } else if (userResponse === 2) {
            $scope.imagePath3 = "img/frequency-pattern-bird.png";
          }
          checkChaStatus();
        }
      }
    };

    // Delete image and user input when user clicks the delete button
    $scope.deleteResponse = function(userResponse) {
      $scope.buttonsDisabled = false;
      if ($scope.patternEntered.length === 1) {
        $scope.imagePath1 = "img/frequency-pattern-blank.png";
      }
      if ($scope.patternEntered.length === 2) {
        $scope.imagePath2 = "img/frequency-pattern-blank.png";
      }
      if ($scope.patternEntered.length === 3) {
        $scope.imagePath3 = "img/frequency-pattern-blank.png";
      }
      $scope.patternEntered = $scope.patternEntered.slice(0, -1);
      clearChosenSelections();
    };

    // Reset page result and GUI parameters for a new presentation
    function resetResults() {
      var presentationId = page.result.presentationId;
      $scope.presentationCount += 1;
      $scope.patternEntered = [];
      $scope.imagePath1 = "img/frequency-pattern-blank.png";
      $scope.imagePath2 = "img/frequency-pattern-blank.png";
      $scope.imagePath3 = "img/frequency-pattern-blank.png";
      clearChosenSelections();
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        presentationId: presentationId,
        responseStartTime: new Date(),
        presentationCount: $scope.presentationCount
      };
    }
  });
