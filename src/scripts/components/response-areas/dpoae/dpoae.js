/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.dpoae", [])
  .directive("dpoaeExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/dpoae/dpoae.html",
      controller: "DPOAEExamCtrl"
    };
  })
  .controller("DPOAEExamCtrl", function($scope, $q, cha, page, disk, chaExams, chaResults, logger, examLogic) {
    function startExam() {
      page.dm.isSubmittable = false;
      $scope.page.dm.questionMainText =
        "Running DPOAE exam for F1: " +
        Math.floor(chaExams.examProperties.F1) +
        " and F2: " +
        Math.floor(chaExams.examProperties.F2);
    }

    function showPlot(resultFromThisPage) {
      var data = {
        resultList: []
      };
      data.resultList.push(resultFromThisPage);

      chaExams.state = "results"; // This is what enables the results view (plot)

      // Update page view, get rid of instructions
      page.dm.hideProgressbar = true;
      $scope.page.dm.title = "Results";
      $scope.page.dm.instructionText = "";

      // Only plot if this page was given displayIds.
      if (angular.isDefined(page.dm.responseArea.displayIds)) {
        // Get results for and display a plot of the dpoae data
        var results = chaResults.getPastResults(page.dm.responseArea.displayIds, null);
        results.forEach(aResult => {
          if (aResult.DpLow === angular.undefined) {
            logger.error("No dpLow when getting past dpoae results.");
            $scope.dataError = "No results returned";
            data.resultList = [];
            return;
          }

          // TODO fix this to look at displayId (page properties from the page for which this result is for)?
          // Or, should this not worry about channel and just plot whatever displayIds are given to it?
          // aResult.channel = aResult.channel | aResult.InputChannel | "unknown";

          data.resultList.push(aResult);
        });
      }

      data.chaInfo = chaExams.getChaInfo();
      page.result = $.extend({}, page.result, resultFromThisPage); // hack to put this at the top level of page.result so the above search of displayids will work.
      page.result = $.extend({}, page.result, data);

      // set default 'response' field
      page.result.response = "continue";

      $scope.dpoaeData = chaResults.createDPOAEData(page.result);
      page.dm.isSubmittable = true;
    }

    var waitFunction = function() {
      chaExams.wait
        .forReadyState()
        .then(function() {
          return cha.requestResults();
        })
        .then(showPlot)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    startExam();

    waitFunction();
  })

  .directive("dpoaePlot", function() {
    return {
      restrict: "E",
      template: '<div id="dpoaePlotWindow"></div>',
      controller: "DPOAEPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("DPOAEPlotCtrl", function($scope, d3Services) {
    d3Services.dpoaePlot("#dpoaePlotWindow", $scope.data);
  });
