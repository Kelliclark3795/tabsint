/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiometry-properties", [])
  .directive("audiometryProperties", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/audiometry/audiometry-properties/audiometry-properties.html",
      controller: "AudiometryPropertiesCtrl"
    };
  })

  .controller("AudiometryPropertiesCtrl", function($scope) {});
