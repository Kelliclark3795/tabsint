/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiogram", [])
  .controller("AudiogramCtrl", function($scope, page, chaResults, gettextCatalog) {
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    page.dm.title = gettextCatalog.getString("Audiogram");
    page.dm.questionMainText = "";
    page.dm.instructionText = "";
    $scope.showAudiogram = true;
  })
  .directive("audiogramPlot", [
    "d3Services",
    "chaResults",
    "page",
    function(d3Services, chaResults, page) {
      return {
        restrict: "EA",
        controller: "AudiogramCtrl",
        scope: {
          audiogramData: "="
        },
        link: function(scope, element, attrs) {
          if (angular.isUndefined(scope.audiogramData)) {
            scope.audiogramData = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[1];
          }
          d3Services.audiogramPlot(element[0], scope.audiogramData);
        }
      };
    }
  ]);
