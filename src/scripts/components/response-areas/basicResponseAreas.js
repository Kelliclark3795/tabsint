/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.basic-response-areas", [])

  .controller("BasicChoiceController", function($scope, examLogic, page) {
    // container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {
        "padding-bottom": page.dm.responseArea.verticalSpacing + "px"
      });
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        "padding-right": page.dm.responseArea.horizontalSpacing / 2 + "px",
        "padding-left": page.dm.responseArea.horizontalSpacing / 2 + "px"
      });
    }

    $scope.label = $scope.choice.text || $scope.choice.id;
    $scope.chosen = function() {
      return $scope.choice.id === page.result.response;
    };
    $scope.isCorrect = function() {
      return $scope.choice.correct === true;
    };
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if ($scope.chosen()) {
        btnClass += "btn-default active ";
      } else {
        btnClass += "btn-default ";
      }
      return btnClass;
    };
    $scope.choose = function() {
      // AUTO-SUBMIT:
      page.result.response = $scope.choice.id;
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.responseArea);
      if (page.dm.isSubmittable && page.result.response !== "Other") {
        examLogic.submit = examLogic.submitDefault;
        examLogic.submit();
      }
    };
  })

  .controller("CheckboxChoiceController", function($scope, page) {
    // Set each button's label
    $scope.label = $scope.choice.text || $scope.choice.id;

    // choice container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {
        "padding-bottom": page.dm.responseArea.verticalSpacing + "px"
      });
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        "padding-right": page.dm.responseArea.horizontalSpacing / 2 + "px",
        "padding-left": page.dm.responseArea.horizontalSpacing / 2 + "px"
      });
    }

    // $scope.saveListToResponse defined in checkbox.js response area
    // It returns the page's response as a list
    $scope.saveListToResponse([]);

    // Function to determine if a choice is currently selected. Used for button styling
    $scope.chosen = function() {
      return _.includes($scope.responseAsList(), $scope.choice.id);
    };

    // toggle chosen/unchosen.
    $scope.choose = function() {
      if ($scope.chosen()) {
        $scope.saveListToResponse(_.without($scope.responseAsList(), $scope.choice.id)); //toggle off.
      } else {
        $scope.saveListToResponse(_.union($scope.responseAsList(), [$scope.choice.id])); //toggle off.
      }
    };

    // Is this choice correct?
    $scope.isCorrect = function() {
      return angular.isDefined($scope.choice.correct) ? $scope.choice.correct : false;
    };

    // Determine button styling. Used by checkbox.html
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if (page.dm.responseArea.buttonScheme === "markIncorrect") {
        if ($scope.chosen()) {
          btnClass += "btn-danger ";
        } else {
          btnClass += "btn-success ";
        }
      } else if (page.dm.responseArea.buttonScheme === "markCorrect") {
        if ($scope.chosen()) {
          btnClass += "btn-success ";
        } else {
          btnClass += "btn-danger ";
        }
      } else {
        btnClass += "btn-default ";
        if ($scope.chosen() && !$scope.gradeResponse && !$scope.showCorrect) {
          btnClass += "active ";
        }
        if ($scope.gradeResponse || $scope.showCorrect) {
          btnClass += "btn-disabled ";
        }
        if (($scope.gradeResponse || $scope.showCorrect) && $scope.chosen() && $scope.isCorrect()) {
          btnClass += "btn-success ";
        }
        if (($scope.gradeResponse || $scope.showCorrect) && $scope.chosen() && !$scope.isCorrect()) {
          btnClass += "btn-danger ";
        }
        if ($scope.showCorrect && !$scope.chosen() && !$scope.isCorrect()) {
          btnClass += "btn-faded ";
        }
      }
      return btnClass;
    };
  })

  .controller("QSINController", function($scope) {
    $scope.label = $scope.choice.text || $scope.choice.id;

    $scope.saveListToResponse([]);

    $scope.chosen = function() {
      return _.includes($scope.responseAsList(), $scope.choice.id);
    };
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if ($scope.chosen()) {
        btnClass += "btn-danger"; //'btn-default active ';
      } else {
        btnClass += "btn-success"; //'btn-default ';
      }
      return btnClass;
    };
    $scope.choose = function() {
      // toggle chosen/unchosen.
      if ($scope.chosen()) {
        $scope.saveListToResponse(_.without($scope.responseAsList(), $scope.choice.id)); //toggle off.
      } else {
        $scope.saveListToResponse(_.union($scope.responseAsList(), [$scope.choice.id])); //toggle off.
      }
    };
  });
