/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.log-maxRows", [])

  .factory("changeLog", function($uibModal, logger, disk, notifications) {
    var api = {
      editMaxLogRows: undefined
    };

    /**
     * Check that user is authorized using a modal dialog, and if so, call targetFn.
     * @param  {function} targetFn - callback function upon success
     * @param  {boolean} skipAuthorize - override the authorization (i.e. admin mode)
     */
    api.editMaxLogRows = function() {
      var ModalInstanceCtrl = function($scope, $uibModalInstance) {
        $scope.save = function(maxLogRows) {
          $uibModalInstance.close(maxLogRows);
        };
        $scope.cancel = function() {
          $uibModalInstance.dismiss("cancel");
        };
      };

      var modalInstance = $uibModal.open({
        templateUrl: "scripts/components/log-maxRows/log-maxRows.html",
        controller: ModalInstanceCtrl,
        backdrop: "static"
      });

      modalInstance.result.then(function(maxLogRows) {
        if (maxLogRows) {
          disk.maxLogRows = maxLogRows;
          logger.info("User updated Log limit to: " + disk.maxLogRows);
          console.log("User updated Log limit to: " + disk.maxLogRows);
        } else {
          logger.warn("User tried to update Log limit, entered rows: " + maxLogRows);
          console.log("User tried to update Log limit, entered rows: " + maxLogRows);
        }
      });
    };

    return api;
  });
