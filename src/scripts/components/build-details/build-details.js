/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.build-details", [])

  .directive("buildDetails", function() {
    return {
      templateUrl: "scripts/components/build-details/build-details.html",
      controller: function($scope, devices, config, version) {
        $scope.devices = devices;
        $scope.config = config;
        $scope.version = version;
      }
    };
  });
