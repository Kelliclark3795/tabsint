/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.cordova.tabsintble", [])

  .factory("tabsintble", function($q, logger) {
    var tabsintble = {};

    /**
     * Connect to BLE device
     * @param  {string} id - mac id of device
     * @return {promise}    promise to connect
     */
    tabsintble.connect = function(id, error) {
      var q = $q.defer();

      ble.connect(
        id,
        function(data) {
          q.resolve(data);
        },
        function(exception) {
          logger.error(`ble connect failed with error: ${JSON.stringify(exception)}`);
          try {
            error();
          } catch (e) {
            logger.error(JSON.stringify(e));
          }
          q.reject({ msg: JSON.stringify(exception) }); // see conventions: https://tabsint.org/docs/DeveloperGuide/conventions/
        }
      );

      return q.promise;
    };

    /**
     * Disconnect from BLE device
     * @param  {string} id - mac id of device
     * @return {promise}    promise to disconnect
     */
    tabsintble.disconnect = function(id) {
      var q = $q.defer();

      ble.disconnect(
        id,
        function() {
          q.resolve();
        },
        function(error) {
          logger.error(`ble disconnect failed with error: ${JSON.stringify(error)}`);
          q.reject({ msg: JSON.stringify(error) }); // see conventions: https://tabsint.org/docs/DeveloperGuide/conventions/
        }
      );

      return q.promise;
    };

    /**
     * Start scanning for BLE devices
     * @param  {string[]} services - array of services (strings) to scan for
     * @param  {success} function - function to call back each time a device is found
     * @param  {error} function - function to call back on error
     * @return {undefined}
     */
    tabsintble.startScan = function(services, success, error) {
      ble.startScan(services, success, function(e) {
        logger.error(`ble startScan failed with error: ${JSON.stringify(e)}`);
        error();
      });
    };

    /**
     * Stop scanning for BLE devices
     * @return {promise}    promise to stop scanning
     */
    tabsintble.stopScan = function() {
      var q = $q.defer();

      ble.stopScan(
        function() {
          q.resolve();
        },
        function(error) {
          logger.error(`ble stopScan failed with error: ${JSON.stringify(error)}`);
          q.reject({ msg: JSON.stringify(error) }); // see conventions: https://tabsint.org/docs/DeveloperGuide/conventions/
        }
      );

      return q.promise;
    };

    /**
     * Write data to ble device
     * @param  {string} id             mac id of device
     * @param  {string} service        service on device
     * @param  {string} characteristic characteristic to write to
     * @param  {ArrayBuffer} data           ArrayBuffer of data to write to characteristic
     * @return {promise}                promise to write to device that, if successful, resolves with any returned data
     */
    tabsintble.write = function(id, service, characteristic, data) {
      var q = $q.defer();

      ble.write(
        id,
        service,
        characteristic,
        data,
        function(data) {
          q.resolve(data);
        },
        function(error) {
          logger.error(`ble failed to write with error: ${JSON.stringify(error)}`);
          q.reject({ msg: JSON.stringify(error) }); // see conventions: https://tabsint.org/docs/DeveloperGuide/conventions/
        }
      );

      return q.promise;
    };

    /**
     * Start notifications from BLE device
     * @param  {string} id             mac id of device
     * @param  {string} service        service on device
     * @param  {string} characteristic characteristic to write to
     * @param  {success} function - function to call back each notification
     * @param  {error} function - function to call back on error
     * @return {undefined}
     */
    tabsintble.startNotification = function(id, service, characteristic, success, error) {
      ble.startNotification(id, service, characteristic, success, function(e) {
        logger.error(`ble startNotification failed with error: ${JSON.stringify(e)}`);
        try {
          error();
        } catch (e) {
          logger.error(JSON.stringify(e));
        }
      });
    };

    /**
     * Stop notifications from BLE device
     * @param  {string} id             mac id of device
     * @param  {string} service        service on device
     * @param  {string} characteristic characteristic to write to
     * @return {Promise}
     */
    tabsintble.stopNotification = function(id, service, characteristic) {
      var q = $q.defer();

      ble.stopNotification(
        id,
        service,
        characteristic,
        function() {
          q.resolve();
        },
        function(e) {
          logger.error(`ble stopNotification failed with error: ${JSON.stringify(e)}`);
          q.reject();
        }
      );

      return q.promise;
    };

    return tabsintble;
  });
