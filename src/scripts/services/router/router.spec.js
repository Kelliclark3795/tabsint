/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Router", function() {
  var router;

  beforeEach(
    angular.mock.inject(function(_router_) {
      router = _router_;
    })
  );

  it("should be initialized to WELCOME", function() {
    expect(router.page).toEqual("WELCOME");
  });
});
