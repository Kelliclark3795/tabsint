/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import * as tv4 from "tv4";

import "../../app";

/* global tabsint */

beforeEach(angular.mock.module("tabsint"));

describe("CHA Results", function() {
  var chaResults, protocol, results, examLogic, config, page, mockTestResults, $rootScope;
  beforeEach(
    angular.mock.inject(function(
      $injector,
      _chaResults_,
      _config_,
      _examLogic_,
      _protocol_,
      _results_,
      _page_,
      _$rootScope_
    ) {
      _results_.current = { testResults: { responses: mockTestResults } };
      _page_.result = {};
      chaResults = _chaResults_;
      config = _config_;
      page = _page_;
      examLogic = _examLogic_;
      protocol = _protocol_;
      results = _results_;
      $rootScope = _$rootScope_;
    })
  );

  it("resultsList should have unique elements ", function() {
    // Assume we have full set of results (no skipped tests). Values based on audiometry protocol.json
    var idList = ["HW_1", "HW_2", "HW_3", "HW_4", "HW_5", "HW_6", "HW_7", "HW_8", "HW_9", "HW_10", "HW_11", "HW_s12"];
    var these_results = [];
    idList.forEach(element => {
      these_results.push({ examType: "HughsonWestlake", presentationId: element });
    });
    angular.mock.inject(function($injector, _results_) {
      _results_.current = { testResults: { responses: these_results } };
      results = _results_;
    });
    var output = chaResults.getPastResults(idList);
    expect(output.length).toEqual(12);
    // Ensure no dupes
    var unique_elements = new Set(output);
    expect(output.length).toEqual(unique_elements.size);
  });
});
