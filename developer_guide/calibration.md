# Calibration Server

### Purpose

All sound files used in exams must be calibrated for the TabSINT hardware.  This enables high quality playback with flat frequency response at a known absolute or relative level on any TabSINT device.

### Process

The calibration is defined in the `calibration` block of the `protocol.json` file (see below).  The waveform in each sound file must be:

1. Analyzed so that it may be played back at a specified absolute or relative level
2. Re-sampled to the hardware optimal sampling rate (48 kHz)
3. Scaled to maximize the available dynamic range on playback (16 bit)
4. Filtered to compensate for the hardware frequency response.

Because the calibration is hardware-specific, each hardware combination (headset and tablet) must use protocol sound files calibrated specifically for that hardware combination.  Upon upload of a protocol, the sound files are calibrated for the headset and tablet specified in the protocol, and saved on the server for distribution to the tablets. A *flat* calibration option is also available if the user prefers to filter the sound files prior to upload. If no tablet is specified in the protocol, Nexus 7 is used. If no headset is specified in the protocol, Vic Firth is used. If the tablet and headset combination requesting the protocol is different than what the server has previously calibrated for, the server must generate a new version of the sound files calibrated for that specific hardware combination.  This may cause a slight delay in the retrieval process. The only parameter which is calculated in real-time during an exam is playback volume level; this minimizes execution time on the tablet and enables responsive playback.

### Playback Modes ###

Before calibration can be defined for a given sound file, you must consider how the file will be played back. TabSINT can play sound files at a specified level, either absolute or relative to a reference file and possibly modified by a standard audiometric weighting function.

There are three distinct playback modes:

1. **Arbitrary:** In this case a sound file is played with a volume such that the A, C, or Z weighted LEQ of the output is equal to a specified target.
2. **As-recorded:** In this case, a reference sound file with a known, fixed LEQ (specified at upload time) must accompany the target sound file. The playback volume is calculated such that the output LEQ of the reference file would be equal to the known value, then the target sound file is played at that volume. This allows playback at *real-world* recorded levels.
3. **WRT-reference:** This playback mode is similar to method 2, except that the level of the reference file is specified at playback time instead of at upload time, and can vary between playbacks. This allows a *corpus* of sound files to be played at various levels relative to a common reference, as in hearing in noise testing (HINT).

### Calibration Definition ###

The way a sound file is calibrated is inferred from the accompanying data at upload time. The three categories of accompanying data and their valid playback modes are:

|            Accompanying data           |   Valid playback modes   |
|:--------------------------------------:|:------------------------:|
|                  none                  |         arbitrary        |
| referenceFile, referenceLevel (dB SPL) |  arbitrary, as-recorded  |
|           referenceFile only           | arbitrary, WRT-reference |

The calibration may also be accompanied by the *calibrationFilter* property, which may take values of *full* or *flat*. If this property is not specified, the default value of *full* is inferred. In a *full* calibration, the sound file is filtered for the frequency response of the specified headset. In a *flat* calibration, the sound file is not filtered, and levels are calculated based on the hardware response at 1 kHz.

### Playback Definition ###

Different inputs are required at playback time for the three playback modes.
The table below summarizes the required (**R**), optional (**O**), and
dis-allowed (**X**) inputs for each mode.

| Input field       | Default | arbitrary | as-recorded | WRT-reference |
|-------------------|---------|-----------|-------------|---------------|
| path              |         | R         | R           | R             |
| playbackMethod    |         | R         | R           | R             |
| targetSPL         |         | R         | X           | R             |
| weighting         | Z       | O         | X           | O             |
| numReplaysAllowed | 0       | O         | O           | O             |
| startTime         | 0       | O         | O           | O             |
| endTime           | inf     | O         | O           | O             |

For *arbitrary* playback, *targetSPL* and *weighting* apply directly to the audio output. For *as-recorded* and *WRT-Reference*, these values apply to the reference file. The detailed implementation of the playback block can be found in the examples and schema below.

### Protocol JSON Calibration and Playback Blocks ###

Every *protocol.json* that includes sound files must include exactly one `calibration` block which lists all the sound files which are to be used.  The calibration block may include one or more `wavfiles` blocks with different categories of accompanying data, but each individual *WAV* file may only be included once.  Note that TabSINT only accepts sound files in the *WAV* format.  These files must be included in the protocol zip archive, either in the root directory or sub-directories.

Playback is defined by *wavfiles* blocks within the exam *pages*. To play multiple files simultaneously, simply list them together as shown in the example. Both the calibration and playback blocks must conform to the schema listed at the end of this document.

A very simple protocol which demonstrates calibration and playback in all three modes is shown below:
   
```
{
   "title":"Demonstrate calibration.",
   "calibration": [
      {
         "wavfiles": ["arbitrarySound1.wav"]
      },
      {
         "wavfiles": [
            "soundfiles/asRec/sound1.wav",
            "soundfiles/asRec/sound2.wav" ],
         "referenceFile": "soundfiles/asRec/referenceSound.wav",
         "referenceLevel": 70
      },
      {
         "wavfiles": [
            "soundfiles/WRTRef/sound1.wav",
            "soundfiles/WRTRef/sound2.wav" ],
         "referenceFile": "soundfiles/WRTRef/referenceSound.wav",
         "calibrationFilter": "flat"
      }
   ],

   "pages":[
      {
         "id":"arb1",
         "title":"Playback arbitrary 1",
         "questionMainText":"Playing arbitrary 1.",
         "wavfiles":[
            {
               "path":"arbitrarySound1.wav",
               "playbackMethod":"arbitrary",
               "targetSPL":"94",
               "weighting":"A"
            }
         ]
      },
      {
         "id":"asRec1",
         "title":"Playback as-recorded mix",
         "questionMainText":"Playing as-recorded mix.",
         "wavfiles":[
            {
               "path":"soundfiles/asRec/sound1.wav",
               "playbackMethod":"as-recorded"
            },
            {
               "path":"soundfiles/asRec/sound2.wav",
               "playbackMethod":"as-recorded"
            }
         ]
      },
      {
         "id":"WRTRef1",
         "title":"Playback WRT-reference with options",
         "questionMainText":"Playing WRT-reference with options.",
         "wavfiles":[
            {
               "path":"soundfiles/WRTRef/sound1.wav",
               "playbackMethod":"WRT-reference",
               "targetSPL":"70",
               "numReplaysAllowed":2,
               "startTime":5.0,
               "endTime":6.0
            }
         ]
      }
   ]
}
```

The schema for the `calibration` block and `wavfiles` playback block can be found in the [protocol schema](../src/res/protocol/schema/protocol_schema.json).

