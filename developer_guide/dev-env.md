# Development Environment

To build and develop the TabSINT software, you must set up your local machine with the appropriate developer dependencies.

Confirm you have the following tools:

- [Git](https://git-scm.com/)
- [Node](https://nodejs.org/)
    - Make sure global `node_modules` directory is on your system path
    - **Node** comes with a command line package manager `npm` 

**NOTE: when installing git for Windows, I had to add a couple extra things to my path, per this [stackoverflow post](https://stackoverflow.com/a/50833818)**

## Setting up TabSINT and its Dependencies

Once you have the required tools, clone this git repository somewhere on your local machine and enter the `tabsint` directory. If you are uncomfortable using git from the command line, we recommend [SourceTree](https://www.sourcetreeapp.com/).

From the `tabsint` directory, run the following command to install dependencies (npm modules and bower components):

```bash
$ npm install
```

At this point, you are ready to serve the app locally in the browser for testing.

### Tablet Dependencies

A few extra tools are necessary to build the app for mobile devices:

#### Android

To build an android package you need the following tools:

- [JAVA JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
  - Currently requires Java JDK 1.8 
  - Confirm that you have the environment variable `JAVA_HOME` set to be the root of the JDK directory (i.e. `C:\Program Files\Java\jdk1.8.0_144`)
  - **NOTE: oracle's download page seems broken (October 26, 2020), so I used [this](https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=hotspot) to install the JDK**
- [Android Studio](https://developer.android.com/sdk/index.html)
  + Note down the path where the Android SDK is installed
  + Make sure the following directories within the Android SDK root directory are on your system path:
    * `[path-to-sdk]/tools/`
    * `[path-to-sdk]/platform-tools/`
    * `[path-to-sdk]/build-tools/[sdk-version]/` where `[sdk-version]` is the SDK version you have installed (30 as of TabsINT v4.3.0).
- [Gradle](https://gradle.org/install/)
  + Current version on continuous integration jobs is 7.3.3.
  + Note down the path where Gradle is installed
  + Make sure the bin directory within your Gradle installation is on your system path.
  

#### iOS

To build an iOS package, you must be developing on a mac with the current build of XCode. Additionally, you should ensure that you have the XCode command-line-tools. Get them by entering the following into a Terminal:

```bash
$ xcode-select --install
```

Cordova may have additional requirements. To find these and how to satisfy them, type the following into the Terminal:

```bash
$ npm run cordova -- requirements
```
