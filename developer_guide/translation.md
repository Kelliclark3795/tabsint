# TabSINT Translations

TabSINT supports translations in the `gettext` format using the [`angular-gettext`](https://angular-gettext.rocketeer.be/) library. To add a translation:

- Use [Poedit](https://poedit.net/) to open `translations/extract.pot`
- Create a new translation file `language.po` in the `translations` directory.
- Run `npm run compile-translations` and build the application.

To extend the translation support in the code base:

- Add the `translate` directive to any HTML tag that you want to translate.
- After adding the tag, run the script `npm run extract-translations`
- Add the appropriate translations in Poedit using `Update from POT file` from the `Catalog` menu, then opening `extract.pot` 
- To view the newly added HTML tags to translate, select `Untranslated entries first` from the `View` menu
- Run `npm run compile-translations`