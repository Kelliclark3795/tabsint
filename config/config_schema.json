{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "TabSINT Configuration Schema",
  "additionalProperties": false,
  "properties": {
    "description": {
      "type": "string",
      "description": "Brief description of the config file, if necessary"
    },
    "build": {
      "type": "string",
      "description": "The build name attached to this verison of TabSINT"
    },
    "permissions": {
      "type": "array",
      "description": "Array of extra android permissions to request at runtime. Inputs should be string keys from the cordova.plugins.permissions object from the android-permissions plugin (i.e. 'RECORD_AUDIO'). See https://github.com/NeoLSN/cordova-plugin-android-permissions"
    },
    "filename": {
      "type": "string",
      "description": "Filename of this configuration file. Automatically added durig the build process"
    },
    "rev": {
      "type": "string",
      "description": "Git revision of the build. Automatically added durig the build process"
    },
    "protocols": {
      "type": "array",
      "description": "List of protocols included in the \"src/protocols/\" directory. These protocols will be installed when the app loads, which can be useful for debugging protocols in the browser."
    },
    "releases": {
      "type": "string",
      "description": "URL of the page hosting releases."
    },
    "cordovaPlugins": {
      "type": "array",
      "description": "Array of cordova plugins to be included with TabSINT build",
      "items": {
        "type": "object",
        "description": "Cordova plugin to include with TabSINT build",
        "required": ["src", "package"],
        "additionalProperties": false,
        "properties": {
          "package": {
            "type": "string",
            "description": "package name of the cordova plugin. For plugins in the cordova plugin registry, this will be the same as defined  in 'src'. This is name used when uninstalling the cordova plugin."
          },
          "src": {
            "type": "string",
            "description": "cordova plugin name or repository to install (i.e. `cordova-plugin-device` or `https://github.com/plugin.git`). This location must contain the proper cordova plugin definition files."
          }
        }
      }
    },
    "tabsintPlugins": {
      "type": "object",
      "description": "Object containing plugins to be included with tabSINT build. Plugins are defined by the key used in this object",
      "additionalProperties": {
        "type": "object",
        "description": "The valid properties for each defined plugin",
        "additionalProperties": false,
        "properties": {
          "version": {
            "type": "string",
            "description": "Version (tag) of the tabsint plugin/docs to add. If the 'src' path is a git repository, this version must coorespond to the current tag on the git repository unless 'debug' is true."
          },
          "debug": {
            "type": "boolean",
            "description": "if true, the build script will ignore the version while building tabsint"
          },
          "src": {
            "type": "string",
            "description": "local path to app plugin source code"
          },
          "params": {
            "type": "object",
            "description": "general object to accept configuration parameters for the plugin"
          }
        }
      }
    },
    "validateProtocols": {
      "type": "boolean",
      "description": "Whether the protocol should be validated upon loading.",
      "default": false
    },
    "server": {
      "type": "string",
      "description": "Type of server to use - use localServer if you want to load and save files directly to the tablet",
      "enum": ["tabsintServer", "gitlab", "localServer"],
      "default": "gitlab"
    },
    "tabsintServer": {
      "type": "object",
      "description": "TabSint server configuration for protocols",
      "properties": {
        "site": {
          "type": "string",
          "description": "Site name to download the protocol from on the TabSINT server"
        },
        "url": {
          "type": "string",
          "description": "Tabsint server url"
        },
        "username": {
          "type": "string",
          "description": "Tabsint server username"
        },
        "password": {
          "type": "string",
          "description": "Tabsint server password"
        }
      }
    },
    "gitlab": {
      "type": "object",
      "description": "Gitlab integration properties",
      "properties": {
        "host": {
          "type": "string",
          "description": "Host url of gitlab account"
        },
        "group": {
          "type": "string",
          "description": "Gitlab group to look for repositories"
        },
        "repository": {
          "type": "string",
          "description": "Gitlab repository to look for protocol"
        },
        "token": {
          "type": "string",
          "description": "Gitlab token (password) to use to access repository"
        },
        "version": {
          "type": "string",
          "description": "Gitlab repository tag or commit to download the protocol from. Leave blank to download the latest tag/commit from the repository."
        },
        "onlyTrackTags": {
          "type": "boolean",
          "description": "Option to track changes to protocol files in Gitlab repository based on tags (true) or commits (false.",
          "default": true
        },
        "resultsGroup": {
          "type": "string",
          "description": "Gitlab group that contains the results repository. This group must use the host and token specified in this object."
        },
        "resultsRepo": {
          "type": "string",
          "description": "Name of the Gitlab repository where results will be uploaded. This repository mush use the same host and token specified in this object."
        }
      }
    },
    "localServer": {
      "protocolDir": {
        "type": "string",
        "description": "Name of the local directory to load the local TabSINT protocol from."
      }
    },
    "adminSettings": {
      "type": "object",
      "description": "List of parameters used to configure TabSINT using a Qr Code",
      "properties": {
        "defaultHeadset": {
          "type": "string",
          "enum":["None","EPHD1", "VicFirth", "VicFirthS2", "HDA200", "WAHTS", "Audiometer"],
          "description": "Headset to use during the exam.",
          "default": "None"
        },
        "language": {
          "type": "string",
          "enum": ["en", "ja", "fr", "sp"],
          "description": "TabSINT display language",
          "default": "English"
        },
        "adminMode": {
          "type": "boolean",
          "description": "Operate TabSINT in admin mode?",
          "default": false
        },
        "adminPin": {
          "type": "number",
          "description": "Pin to access TabSINT when not in admin mode",
          "default": 7114
        },
        "disableLogs": {
          "type": "boolean",
          "description": "Disable logging feature, including upload to the TabSINT server?",
          "default": true
        },
        "maxLogRows": {
          "type": "number",
          "description": "Limit the length of the logger DB to maximum number of records.",
          "default": 1000
        },
        "disableAutomaticVolumeControl": {
          "type": "boolean",
          "description": "Disable TabSINT control of the tablet volume",
          "default": false
        },
        "enableSkipExam": {
          "type": "boolean",
          "description": "Enable ability to skip exam",
          "default": false
        },
        "automaticallyOutputTestResults": {
          "type": "boolean",
          "description": "If true, results are automatically exported or uploaded upon submission. If false, results are stored in TabSINT until they are manually exported or uploaded.",
          "default": false
        },
        "resultsMode": {
          "type": "string",
          "enum": ["Upload/Export", "Upload Only", "Export Only"],
          "description": "Mode to upload and/or export results. Upload allows saving results to Gitlab or the TabSINT server, while export saves the results locally.",
          "default": "Upload/Export"
        },
        "localResultsDirectory": {
          "type": "string",
          "desctiption": "Directory to save results in locally, on the tablet",
          "default": "tabsint-results"
        },
        "tabletGain": {
          "type": "number",
          "description": "",
          "default": -8.56
        },
        "headsetType": {
          "type": "string",
          "enum": ["Bluetooth 3.0", "Bluetooth 2.0", "USB Host"],
          "description": "",
          "default": "Bluetooth 3.0"
        },
        "disableAudioStreaming": {
          "type": "boolean",
          "description": "Disable audio streaming feature of the WAHTS",
          "default": false
        },
        "ignoreFirmwareUpdates": {
          "type": "boolean",
          "description": "Ignore firmware update messages when connecting to the WAHTS.",
          "default": false
        },
        "shutdownTimer": {
          "type": "number",
          "description": "Time in minutes WAHTS should stay powered on.",
          "default": 60
        },
        "enableHeadsetMediaManagement": {
          "type": "boolean",
          "description": "",
          "default": false
        },
        "requireEncryptedResults": {
          "type": "boolean",
          "description": "Require public key to be defined in protocol so all output results are encrypted",
          "default": false
        },
        "recordTestLocation": {
          "type": "boolean",
          "description": "Record the latitude and longitude of the test location and store in test results",
          "default": false
        }
      }
    }
  }
}
